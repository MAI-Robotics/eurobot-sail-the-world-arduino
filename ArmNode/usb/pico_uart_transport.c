#include <stdio.h>
#include <pico/stdlib.h>

#include "tusb.h"

#include <uxr/client/profile/transport/custom/custom_transport.h>

void usleep(uint64_t us)
{
    sleep_us(us);
}

int clock_gettime(clockid_t unused, struct timespec *tp)
{
    uint64_t m = time_us_64();
    tp->tv_sec = m / 1000000;
    tp->tv_nsec = (m % 1000000) * 1000;
    return 0;
}

bool pico_serial_transport_open(struct uxrCustomTransport * transport)
{
    stdio_init_all();
    return true;
}

bool pico_serial_transport_close(struct uxrCustomTransport * transport)
{
    return true;
}

size_t pico_serial_transport_write(struct uxrCustomTransport * transport, uint8_t *buf, size_t len, uint8_t *errcode)
{
    uint8_t *copyBuffer;
    for (size_t i = 0; i < len; i++)
    {
        uint8_t character = buf[i];
        copyBuffer[i] = character;
        tud_cdc_write_char(character);
    }
    len = tud_cdc_write_flush();
    tud_task();
    return len;
}

size_t pico_serial_transport_read(struct uxrCustomTransport * transport, uint8_t *buf, size_t len, int timeout, uint8_t *errcode)
{
    tud_task();
    if (tud_cdc_available())
    {
        // Is there any data from the host for us to tx
        uint tx_len = tud_cdc_read(buf, sizeof(buf));
        return tx_len;
    }
    *errcode = 0;
    return 0;
}
