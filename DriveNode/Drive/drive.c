#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>

#include "pico/stdlib.h"
#include "hardware/gpio.h"

#include "drive.h"
#include "../Config.h"
#include "../SecondCore/SecondCore.h"
#include "../util/Types.h"
#include "driveUtils.h"

uint stepsTaken = 0;
uint driveState = 0;
volatile bool interruptDrive = false;

static const uint deccelerationPhase = 100;
static const float deccelerationAmount = 0.025;
static const float accelerationAmount = 0.0025;
static const uint accelerationPhase = 400;

static inline void performStepSynchronous(float a) {
    gpio_put(STEPPER_RIGHT_PUSE, true);
    gpio_put(STEPPER_LEFT_PULSE, true);
    busy_wait_us(TIME_PER_STEP_US + a);
    gpio_put(STEPPER_RIGHT_PUSE, false);
    gpio_put(STEPPER_LEFT_PULSE, false);
    busy_wait_us(TIME_PER_STEP_US + a);
}

static void moveSynchronous(DriveData *data) {
    float a = 0; //for acceleration
    for (stepsTaken = 0; stepsTaken < data->steps; stepsTaken++)
    {
        uint steps = data->steps;
        a = 0;
        if (interruptDrive) {
            if (steps - stepsTaken > deccelerationPhase) {
                for (uint i = 0; i < deccelerationPhase; i++) {
                    a = TIME_PER_STEP_US * deccelerationAmount * (deccelerationPhase - i);
                    performStepSynchronous(a);
                }
                stepsTaken += deccelerationPhase;
                break;
            }
        }

        
        if (stepsTaken < accelerationPhase && stepsTaken <= steps / 2) // Acceleration
        {
            a = TIME_PER_STEP_US * accelerationAmount * (accelerationPhase - stepsTaken);
        }

        if (steps - stepsTaken < accelerationPhase && stepsTaken > steps / 2) // Decceleration
        {
            a = TIME_PER_STEP_US * accelerationAmount * (accelerationPhase - (steps - stepsTaken));
        }

        performStepSynchronous(a); 
    }
}

void turnFast(DriveData *data)
{
    bool dir = data->direction;
    gpio_put(STEPPER_LEFT_DIRECTION, dir ? 1 : 0);
    gpio_put(STEPPER_RIGHT_DIRECTION, dir ? 1 : 0);
    gpio_put(STEPPER_ENABLE, false);

    busy_wait_us(1000);

    moveSynchronous(data);

    gpio_put(STEPPER_ENABLE, true);
}

void driveFast(DriveData *data)
{
    bool dir = data->direction;
    gpio_put(DEBUG_BUZZER, 1);
    gpio_put(STEPPER_LEFT_DIRECTION, dir ? 1 : 0);
    gpio_put(STEPPER_RIGHT_DIRECTION, dir ? 0 : 1);
    gpio_put(STEPPER_ENABLE, false);

    busy_wait_us(1000);

    moveSynchronous(data);

    gpio_put(STEPPER_ENABLE, true);
    gpio_put(DEBUG_BUZZER, 0);
}
