#ifndef DRIVE_NODE_H
#define DRIVE_NODE_H
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

extern const uint LED_PIN;

extern bool ledState;

extern void fifo_irq_core0();
extern void error();

#endif // DRIVE_NODE_H