#pragma once
#ifndef SECONDCORE_H
#define SECONDCORE_H
#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include "pico/stdio.h"
#include "pico/multicore.h"

#include "../Drive/drive.h"

    void initMulticore();

////////////
// FLAGS //
//////////
#define DRIVE_READY_FLAG 0x2EAD7000     // Second to First Core
#define DRIVE_BUSY_FLAG 0xB10C8000      // Second to First Core
#define DRIVE_INTERRUPT_FLAG 0x57090000 // First to Second Core
#define DRIVE_DONE_FLAG 0xD02E0000      // Second to First Core

#define DATA_READY_FLAG 0xDA7A2EAD     // First to Second Core
#define DATA_OK_FLAG 0xDA7A0800        // Second to First Core
#define DATA_MISSMATCH_FLAG 0xF0C8ED00 // First to Second Core

#ifdef __cplusplus
}
#endif
#endif // SECONDCORE_H