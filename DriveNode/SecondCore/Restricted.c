#include <stdio.h>
#include <string.h>

#include "pico/stdio.h"
#include "pico/multicore.h"
#include "pico/malloc.h"
#include "Restricted.h"

critical_section_t drive_aim_lock;

DriveData _driveData;

DriveData getDriveData()
{
    DriveData data;
    critical_section_enter_blocking(&drive_aim_lock);
    memcpy(&data, &_driveData, sizeof(_driveData));
    critical_section_exit(&drive_aim_lock);
    return data;
}

void setDriveData(DriveData data) {
    critical_section_enter_blocking(&drive_aim_lock);
    memcpy(&_driveData, &data, sizeof(data));
    critical_section_exit(&drive_aim_lock);
}