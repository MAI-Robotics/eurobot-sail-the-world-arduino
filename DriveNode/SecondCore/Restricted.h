#ifndef RESTRICTED_H
#define RESTRICTED_H
#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include "pico/multicore.h"
#include "pico/mutex.h"
#include "../util/Types.h"

    DriveData getDriveData();
    void setDriveData(DriveData data);

    /////////////////
    // SEMAPHORES //
    ///////////////

    // extern semaphore_t drive_aim_sem;

    //////////////
    // MUTEXES //
    ////////////

    // extern mutex_t drive_aim_lock;

    ////////////////////////
    // CRITICAL SECTIONS //
    //////////////////////
    extern critical_section_t drive_aim_lock;

#ifdef __cplusplus
}
#endif
#endif // RESTRICTED_H