#pragma once
#ifndef ROS_H
#define ROS_H
#ifdef __cplusplus
extern "C"
{
#endif

#include <rcl/rcl.h>
#include <rcl/error_handling.h>
#include <rclc/rclc.h>
#include <rclc/executor.h>
#include <std_msgs/msg/int32.h>
#include <std_msgs/msg/empty.h>
#include <rmw_uros/options.h>

    void publishDistanceDriven(int32_t distance);

    int initNode(rcl_node_t *node, rcl_allocator_t *allocator, rclc_support_t *support);
    int initExecutor(rclc_executor_t *executor, rcl_allocator_t *allocator, rclc_support_t *support);

    int initPubs(rcl_node_t *node);
    int initSubs(rcl_node_t *node);

#ifdef __cplusplus
}
#endif
#endif // ROS_H